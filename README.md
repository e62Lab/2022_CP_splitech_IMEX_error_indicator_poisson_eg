# IMEX error indicator test on a Poisson problem

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.9 (or higher)
- Jupyter notebooks
- Medusa library (https://e6.ijs.si/medusa/)
- photutils (https://photutils.readthedocs.io/en/stable/)

## Usage

Create or go to `root/build/` directoy and build using

```bash
cmake .. && make -j 12
```

The executable will be created in `root/bin/` directory. The executable must be run with a parameter
with all the settings, e.g.

```bash
./error_indicator ../input/settings.xml
```

Results are written to `root/data/`. A Jupyter notebook is located in `root/analyses/`, it is used to plot graphs.

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

Mitja Jančič,
Filip Strniša

## License

This project is open source and free to use.
