project(p-adaptive)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_CXX_FLAGS "-std=c++17 -O3")

add_subdirectory(${CMAKE_SOURCE_DIR}/../medusa/ medusa)
include_directories(${CMAKE_SOURCE_DIR}/../medusa/include/)

add_executable(error_indicator source/error_indicator.cpp)
target_link_libraries(error_indicator medusa)  # link to our library

