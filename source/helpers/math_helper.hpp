#ifndef MATH_HELPER_HPP
#define MATH_HELPER_HPP

#include <algorithm>

using namespace std;

/**
 * @brief Returns value of Binomial Coefficient C(n, k).
 * https://www.geeksforgeeks.org/binomial-coefficient-dp-9/
 *
 * @param n
 * @param k
 * @return int
 */
int binomialCoeff(int n, int k) {
    int C[n + 1][k + 1];
    int i, j;

    // Calculate value of Binomial Coefficient in bottom up manner.
    for (i = 0; i <= n; i++) {
        for (j = 0; j <= min(i, k); j++) {
            // Base Cases.
            if (j == 0 || j == i) C[i][j] = 1;

            // Calculate value using previously stored values.
            else
                C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
        }
    }

    return C[n][k];
}

/**
 * Sign function.
 * @tparam T
 * @param val
 * @return
 */
template <typename T>
int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

#endif  // MATH_HELPER_HPP
