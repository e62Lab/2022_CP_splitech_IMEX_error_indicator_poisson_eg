#ifndef RUN_PROCEDURE_HPP
#define RUN_PROCEDURE_HPP

#include <medusa/Medusa.hpp>
#include "solve_implicit.hpp"
#include "apply_explicit_operators.hpp"
#include "compute_error_helper.hpp"

/**
 * Run the solution procedure.
 * @tparam vec_t Template parameter - vector.
 * @param conf Configuration XML object.
 * @param hdf HDF output object.
 * @param timer Timer.
 */
template <typename vec_t>
void run_procedure(const mm::XML& conf, mm::HDF& hdf, mm::Timer* timer) {
    // Build domain.
    if (timer) timer->addCheckPoint("domain");
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        std::cout << "Building domain ..." << std::endl;
    }

    // Build ball shape.
    double ball_origin = conf.get<double>("domain.origin");
    double ball_radius = conf.get<double>("domain.radius");
    mm::BallShape<vec_t> ball(ball_origin, ball_radius);

    // Domain discretization.
    double dx = conf.get<double>("domain.dx");
    auto h = [=](const vec_t& p) { return dx; };
    int type_dirichlet = conf.get<int>("domain.boundary_dirichlet");
    int type_neumann = conf.get<int>("domain.boundary_neumann");
    int type_interior = conf.get<int>("domain.interior");

    mm::DomainDiscretization<vec_t> domain = ball.discretizeBoundaryWithDensity(h, type_dirichlet);
    mm::GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(5).seed(conf.get<int>("fill.seed"));
    domain.addInternalNode(0.0, type_interior);
    fill_randomized(domain, h, type_interior);
    int N = domain.size();
    if (debug) {
        prn(N);
    }

    // Node types.
    auto nodes_boundary = domain.boundary();
    for (int i : nodes_boundary) {
        if (domain.pos(i, 0) <= conf.get<double>("domain.neumann_threshold")) {
            domain.type(i) = type_neumann;
        }
    }
    if (debug) {
        int N_BC = (domain.types() == type_neumann).size();
        std::cout << "Number of Neumann BC: " << std::to_string(N_BC) << std::endl;
    }

    // Save domain
    if (timer) timer->addCheckPoint("domain_created");
    hdf.atomic().writeDomain("domain", domain);

    // Engine for implicit.
    auto k = conf.get<int>("approx_implicit.k");
    auto aug = conf.get<int>("approx_implicit.mon_degree");
    mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest> engine_implicit(k, aug);
    // Solve implicit.
    Eigen::VectorXd sol_implicit = solve_implicit<vec_t>(conf, domain, *timer, engine_implicit);
    hdf.atomic().writeDoubleArray("solution_implicit", sol_implicit);

    // Engine for explicit.
    k = conf.get<int>("approx_explicit.k");
    aug = conf.get<int>("approx_explicit.mon_degree");
    mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest> engine_explicit(k, aug);
    // Solve explicit.
    Eigen::VectorXd explicit_operator_field =
        apply_explicit_operators<vec_t>(conf, domain, *timer, sol_implicit, engine_explicit);
    hdf.atomic().writeDoubleArray("solution_explicit", explicit_operator_field);

    // Compute errors.
    // Compute analytic solution.
    Eigen::VectorXd sol_analytic(N);
    Eigen::VectorXd sol_operators_analytic(N);
    for (int k = 0; k < N; k++) {
        int type = domain.type(k);
        vec_t pos = domain.pos(k);

        // Analytic solution to compare with implicit.
        sol_analytic[k] = u_analytic(pos);

        // Analytic operator field solution.
        if (type == type_interior) {
            sol_operators_analytic[k] = u_laplacian(pos);
        } else if (type == type_dirichlet) {
            // Dirichlet.
            sol_operators_analytic[k] = u_analytic(pos);
        } else {
            vec_t normal = domain.normal(k);
            // Neumann.
            sol_operators_analytic[k] = normal.dot(u_gradient(pos));
        }
    }
    // Implicit error.
    double val;
    compute_error(sol_implicit, sol_analytic, "implicit", hdf, val, debug);
    // Explicit error.
    compute_error(explicit_operator_field, sol_operators_analytic, "explicit", hdf, val, debug);
}

#endif /* RUN_PROCEDURE_HPP */
