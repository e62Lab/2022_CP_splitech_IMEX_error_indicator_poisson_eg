#ifndef APPLY_EXPLICIT_OPERATOR_HPP
#define APPLY_EXPLICIT_OPERATOR_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "math_helper.hpp"
#include "poisson_helper.hpp"

/**
 * Applies explicit operators to implicitly computed scalar field. Not adaptive.
 * @tparam vec_t Vector.
 * @tparam approx_t Approximation engine.
 * @param conf Configuration XML file.
 * @param domain Domain.
 * @param timer Timer.
 * @param field Implicitly computed scalar field.
 * @param engine Approximation engine.
 * @return Operator field.
 */
template <typename vec_t, typename approx_t>
Eigen::VectorXd apply_explicit_operators(const mm::XML& conf,
                                         mm::DomainDiscretization<vec_t>& domain, mm::Timer& timer,
                                         Eigen::VectorXd& field, approx_t engine) {
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        std::cout << "Computing explicit error ..." << std::endl;
    }

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }
    int support_size = conf.get<int>("approx_explicit.support_size");
    if (support_size == -1) {
        int mon_degree = conf.get<int>("approx_explicit.mon_degree");
        support_size = 2 * binomialCoeff(mon_degree + vec_t::dim, vec_t::dim);
    }
    domain.findSupport(mm::FindClosest(support_size));

    // Shapes.
    auto storage = domain.template computeShapes<mm::sh::lap | mm::sh::d1>(engine);
    // Construct explicit operators over our storage.
    auto op = storage.explicitOperators();

    int N = field.size();
    Eigen::VectorXd sol(N);
    sol.setZero();

    // Interior.
    for (int i : domain.interior()) {
        sol[i] = op.lap(field, i);
    }
    // Dirichlet BC.
    auto dir = domain.types() == conf.get<int>("domain.boundary_dirichlet");
    for (int i : dir) {
        sol[i] = field[i];
    }
    // Neumann BC.
    auto neu = domain.types() == conf.get<int>("domain.boundary_neumann");
    for (int i : neu) {
        vec_t normal = domain.normal(i);
        vec_t grad = op.grad(field, i);

        sol[i] = op.neumann(field, i, normal, normal.dot(grad));
    }

    return sol;
}

/**
 * Applies explicit operators to implicitly computed scalar field. Adaptive.
 * @tparam vec_t Vector.
 * @tparam approx_t Approximation engine.
 * @param conf Configuration file.
 * @param domain Domain.
 * @param timer Timer.
 * @param field Implicitly obtained field.
 * @param engines Range of approximation engines.
 * @return Explicit operator field.
 */
template <typename vec_t, typename approx_t>
Eigen::VectorXd apply_explicit_operators_mixed_orders(const mm::XML& conf,
                                                      mm::DomainDiscretization<vec_t>& domain,
                                                      mm::Timer& timer, Eigen::VectorXd& field,
                                                      mm::Range<approx_t> engines) {
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        std::cout << "Computing explicit error with mixed orders ..." << std::endl;
    }
    int min_order = conf.get<int>("adaptivity.min_order") + 1;  // Increase order for indicator.
    int max_order = conf.get<int>("adaptivity.max_order") + 1;  // Increase order for indicator.

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }

    // Node types.
    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j++) {
        // Small order because we wish to approximate with increased order.
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j - 1; });
        node_types.push_back(_range);
    }

    // Support sizes.
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size_order = 2 * binomialCoeff(i + min_order + vec_t::dim, vec_t::dim);
            mm::FindClosest f(support_size_order);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>> operators;
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
    // Support shapes.
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i + 1], node_types[i], operators, &storage);
        }
    }

    // Construct explicit operators over our storage.
    if (debug) {
        std::cout << "Constructing operators ..." << std::endl;
    }
    auto op = storage.explicitOperators();

    int N = field.size();
    Eigen::VectorXd sol(N);
    sol.setZero();

    if (debug) {
        std::cout << "Solving ..." << std::endl;
    }
    // Interior.
#pragma omp parallel for
    for (int i : domain.interior()) {
        sol[i] = op.lap(field, i);
    }
    // BC.
    for (int i : domain.boundary()) {
        vec_t pos = domain.pos(i);

        if (pos[0] <= conf.get<double>("domain.neumann_threshold")) {
            // Neumann BC.
            vec_t normal = domain.normal(i);
            vec_t grad = op.grad(field, i);

            sol[i] = op.neumann(field, i, normal, normal.dot(grad));
        } else {
            // Dirichlet BC.
            sol[i] = field[i];
        }
    }

    return sol;
}

#endif /* APPLY_EXPLICIT_OPERATOR_HPP */
