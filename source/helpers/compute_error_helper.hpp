#ifndef COMPUTE_ERROR_HELPER_HPP
#define COMPUTE_ERROR_HELPER_HPP

#include <medusa/Medusa.hpp>

/**
 * Computes l1, l2 and l_infinity norm errors.
 * @param numerical_sol Numerical solution.
 * @param analytic_sol Analytical solution.
 * @param name Name to store in hdf.
 * @param hdf HDF object.
 * @param inf_error Infinity error reference.
 * @param debug Boolean for debug output.
 */
void compute_error(Eigen::VectorXd& numerical_sol, Eigen::VectorXd& analytic_sol, std::string name,
                   mm::HDF& hdf, double& inf_error, bool debug = false) {
    if (debug) {
        cout << "Computing errors ..." << endl;
    }

    // Implicit solution error.
    Eigen::VectorXd err = analytic_sol - numerical_sol;
    hdf.atomic().writeDoubleArray(mm::format("error_%s", name), err);

    // Compute error norm.
    double err_norm_1 = err.lpNorm<1>() / analytic_sol.lpNorm<1>();
    double err_norm_2 = err.lpNorm<2>() / analytic_sol.lpNorm<2>();
    double err_norm_inf = err.lpNorm<Eigen::Infinity>() / analytic_sol.lpNorm<Eigen::Infinity>();
    inf_error = err_norm_inf;

    // Add error norms to hdf.
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_1_%s", name), err_norm_1);
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_2_%s", name), err_norm_2);
    hdf.atomic().writeDoubleAttribute(mm::format("err_norm_inf_%s", name), err_norm_inf);

    if (debug) {
        cout << "err_norm_1_" + name + " = " + to_string(err_norm_1) << endl;
        cout << "err_norm_2_" + name + " = " + to_string(err_norm_2) << endl;
        cout << "err_norm_inf_" + name + " = " + to_string(err_norm_inf) << endl;
    }
}

#endif /* COMPUTE_ERROR_HELPER_HPP */
