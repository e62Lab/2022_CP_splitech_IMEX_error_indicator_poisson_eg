#ifndef SOLVE_IMPLICIT_HPP
#define SOLVE_IMPLICIT_HPP

#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include "math_helper.hpp"
#include <Eigen/IterativeLinearSolvers>
#include "poisson_helper.hpp"

/**
 * Compute solution using implicit operators. Not adaptive (only one approximation order).
 * @tparam vec_t Vector.
 * @tparam approx_t Approximation engine.
 * @param conf Configuration XML file.
 * @param domain Domain.
 * @param timer Timer.
 * @param engine Approximation engine.
 * @return
 */
template <typename vec_t, typename approx_t>
Eigen::VectorXd solve_implicit(const mm::XML& conf, mm::DomainDiscretization<vec_t>& domain,
                               mm::Timer& timer, approx_t engine) {
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        std::cout << "Solving implicit ..." << std::endl;
    }

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }
    int support_size = conf.get<int>("approx_implicit.support_size");
    if (support_size == -1) {
        int mon_degree = conf.get<int>("approx_implicit.mon_degree");
        support_size = 2 * binomialCoeff(mon_degree + vec_t::dim, vec_t::dim);
    }
    domain.findSupport(mm::FindClosest(support_size));

    // Shapes.
    auto storage = domain.template computeShapes<mm::sh::lap | mm::sh::d1>(engine);

    int N = domain.size();
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);
    rhs.setZero();
    M.reserve(storage.supportSizes());

    // construct implicit operators over our storage
    auto op = storage.implicitOperators(M, rhs);

    for (int i : domain.interior()) {
        op.lap(i) = u_laplacian(domain.pos(i));
    }
    // Dirichlet BC.
    auto dir = domain.types() == conf.get<int>("domain.boundary_dirichlet");
    for (int i : dir) {
        op.value(i) = u_analytic(domain.pos(i));
    }
    // Neumann BC.
    auto neu = domain.types() == conf.get<int>("domain.boundary_neumann");
    for (int i : neu) {
        vec_t normal = domain.normal(i);
        vec_t pos = domain.pos(i);

        vec_t grad = u_gradient(pos);
        op.neumann(i, normal) = normal.dot(grad);
    }

    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.compute(M);
    auto u = solver.solve(rhs);

    if (solver.info() != Eigen::Success) {
        mm::print_red("Solver did not converge!");
    }

    return u;
}

/**
 * Compute solution using implicit operators.  Adaptive (multiple approximation orders).
 * @tparam vec_t Vector.
 * @tparam approx_t Approximation engine.
 * @param conf Configuration XML file.
 * @param domain Domain.
 * @param timer Timer.
 * @param engines Range of engines.
 * @param guess Initial guess for bicgstab.
 * @return Implicit solution with mixed approximation orders.
 */
template <typename vec_t, typename approx_t>
Eigen::VectorXd solve_implicit_mixed_orders(const mm::XML& conf,
                                            mm::DomainDiscretization<vec_t>& domain,
                                            mm::Timer& timer, mm::Range<approx_t>& engines,
                                            Eigen::VectorXd& guess) {
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        std::cout << "Solving implicit with mixed orders..." << std::endl;
    }
    int min_order = conf.get<int>("adaptivity.min_order");
    int max_order = conf.get<int>("adaptivity.max_order");

    // Find support.
    if (debug) {
        std::cout << "Finding support ..." << std::endl;
    }

    // Node types.
    mm::Range<mm::Range<int>> node_types;
    for (int j = min_order; j <= max_order; j++) {
        mm::Range<int> _range = domain.types().filter([&](int i) { return abs(i) == j; });
        node_types.push_back(_range);
    }

    // Support sizes.
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            int support_size_order = 2 * binomialCoeff(i + min_order + vec_t::dim, vec_t::dim);
            mm::FindClosest f(support_size_order);
            f.forNodes(node_types[i]);
            domain.findSupport(f);
        }
    }

    // Shapes.
    if (debug) {
        std::cout << "Computing shapes ..." << std::endl;
    }
    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>> operators;
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
    // Support shapes.
    for (int i = 0; i < node_types.size(); i++) {
        if (!node_types[i].empty()) {
            computeShapes(domain, engines[i], node_types[i], operators, &storage);
        }
    }

    int N = domain.size();
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);
    rhs.setZero();
    M.reserve(storage.supportSizes());

    // Construct implicit operators over our storage.
    if (debug) {
        std::cout << "Constructing operators ..." << std::endl;
    }
    auto op = storage.implicitOperators(M, rhs);

    // Interior.
#pragma omp parallel for
    for (int i : domain.interior()) {
        op.lap(i) = u_laplacian(domain.pos(i));
    }
    // BC.
    for (int i : domain.boundary()) {
        vec_t pos = domain.pos(i);

        if (pos[0] <= conf.get<double>("domain.neumann_threshold")) {
            // Neumann BC.
            vec_t normal = domain.normal(i);
            vec_t grad = u_gradient(pos);

            op.neumann(i, normal) = normal.dot(grad);
        } else {
            // Dirichlet BC.
            op.value(i) = u_analytic(domain.pos(i));
        }
    }

    string solver_type = conf.get<string>("solver.type");
    if (debug) {
        std::cout << "Solving with: " << solver_type << " ..." << std::endl;
    }
    Eigen::VectorXd u(N);
    if (solver_type == "lu") {
        Eigen::SparseLU<decltype(M)> solver;
        solver.compute(M);
        Eigen::VectorXd sol = solver.solve(rhs);

        if (solver.info() == Eigen::Success) {
            if (debug) {
                mm::print_green("Solver converged!\n");
            }
        } else {
            mm::print_red("Solver did not converge!\n");
            Eigen::VectorXd sol = guess;
        }
        u = sol;
    } else if (solver_type == "bicgstab") {
        Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;

        if (conf.get<int>("solver.use_preconditioner") == 1) {
            solver.preconditioner().setFillfactor(conf.get<int>("solver.fill"));
            solver.preconditioner().setDroptol(conf.get<double>("solver.drop_tol"));
            solver.setMaxIterations(conf.get<int>("solver.max_iter"));
            solver.setTolerance(conf.get<double>("solver.global_tol"));
        }
        solver.compute(M);
        Eigen::VectorXd sol = solver.solveWithGuess(rhs, guess);
        if (debug) {
            prn(solver.iterations());
            prn(solver.error());
        }
        u = sol;
    } else {
        assert_msg(false, "Unknown solver type: '%s'", solver_type);
    }

    return u;
}

#endif /* SOLVE_IMPLICIT_HPP */
