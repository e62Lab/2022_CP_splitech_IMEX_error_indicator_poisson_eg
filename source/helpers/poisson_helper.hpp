#ifndef POISSON_HELPER_HPP
#define POISSON_HELPER_HPP

#include <medusa/Medusa.hpp>
#include <math.h>

const double alpha = 1000.0;
const double source_location = 0.5;

// STRONG SOURCE
template <typename vec_t>
typename vec_t::scalar_t u_analytic(const vec_t& p) {
    vec_t source = vec_t::Constant(source_location);
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent);
}

template <typename vec_t>
typename vec_t::scalar_t u_laplacian(const vec_t& p) {
    vec_t source = vec_t::Constant(source_location);
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return 4.0 * exp(exponent) * (mm::ipow<2>(alpha) * squared_norm - alpha);
}

template <typename vec_t>
vec_t u_gradient(const vec_t& p) {
    vec_t source = vec_t::Constant(source_location);
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    double constant = -2 * alpha * exp(exponent);

    return constant * p;
}

#endif  // POISSON_HELPER_HPP
