#include <medusa/Medusa.hpp>
#include "helpers/run_procedure.hpp"

using namespace std;
using namespace mm;

int main(int argc, const char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 hdf to store the parameters.
    int dim = conf.get<int>("domain.dim");
    string output_hdf = conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") +
                        "_dim" + to_string(dim) + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);

    // Write params to results hdf.
    hdf.writeXML("conf", conf);
    hdf.close();

    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;
    switch (dim) {
        case 1:
            run_procedure<Vec1d>(conf, hdf, &t);
            break;
        case 2:
            run_procedure<Vec2d>(conf, hdf, &t);
            break;
        case 3:
            run_procedure<Vec3d>(conf, hdf, &t);
            break;
        default:
            assert_msg(false, "Dimension %d not supported.", dim);
    }

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
